# 1.0.0 (2018-08-10)


### Bug Fixes

* adding package management ([8c0b026](https://gitlab.com/mfgames-writing/mfgames-writing-guillemet-js/commit/8c0b026))
